def fib(n: int) -> int:
    if n <= 1:
        return n
    return fib(n - 1) + fib(n - 2)


def factorial(n: int) -> int:
    if n == 1:
        return n
    return n * factorial(n - 1)


def add_nums(n1: int, n2: int) -> int:
    return n1 + n2
