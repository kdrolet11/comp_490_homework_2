import pytest
import SimpleFunctions


@pytest.mark.parametrize('function_input, expected_output', [(7, 13)])
def test_positive_fib(function_input, expected_output):
    assert SimpleFunctions.fib(function_input) == expected_output


@pytest.mark.parametrize('function_input, expected_output', [(8, 13)])
def test_negative_fib(function_input, expected_output):
    assert SimpleFunctions.fib(function_input) != expected_output


@pytest.mark.parametrize('function_input, expected_output', [(7, 5040)])
def test_positive_factorial(function_input, expected_output):
    assert SimpleFunctions.factorial(function_input) == expected_output


@pytest.mark.parametrize('function_input, expected_output', [(8, 5040)])
def test_negative_factorial(function_input, expected_output):
    assert SimpleFunctions.factorial(function_input) != expected_output
